package springboootdemo.springboot.dbStatement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import springbootdemo.springboot.dbconnection.DBConnection;
import springbootdemo.springboot.entity.Employee;

public class DBOperations {
	DBConnection db= null;
	Connection con=null;
	PreparedStatement ps=null;
	String identifier;
	ResultSet resultSet = null;
	List<Employee> list ;
	
	
	public void insertIntoDB(Employee emp) {
		System.out.println(emp.getFirstname());
		db=new DBConnection();
		con= db.getDBConnection();
		
		if(con!=null) {
			System.out.println(con);
			identifier=emp.getFirstname();
			
			
			try {
				String insertTableSQL = "INSERT INTO mynetapp2.employee"
						+ "(firstname, lastname, email) VALUES"
						+ "(?,?,?)";
				ps = con.prepareStatement(insertTableSQL);
				ps.setString(1,emp.getFirstname());
				ps.setString(2,emp.getLastname());
				ps.setString(3,emp.getEmail());
				ps.execute();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		
	}
	
	public Employee getEmployeeFromDB() {
		Employee emp = new Employee();
		identifier="raj";
		db=new DBConnection();
		con= db.getDBConnection();
		list= new ArrayList<Employee>();
		String fetchResult = "select * from mynetapp2.employee where firstname=?";
		try {
			
			ps = con.prepareStatement(fetchResult);
			ps.setString(1, identifier);
			resultSet=	ps.executeQuery();
			System.out.println(resultSet.next());
			 while(resultSet.next()) {
				 emp.setFirstname( resultSet.getString(1));
				 emp.setLastname(resultSet.getString(2));
				 emp.setEmail(resultSet.getString(3));	
				 list.add(emp);
	                
	            }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		JSONArray jsArray = new JSONArray(list);
		System.out.println(jsArray);
		
		
		return emp;
	}
	

}
