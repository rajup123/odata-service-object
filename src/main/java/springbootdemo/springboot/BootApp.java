package springbootdemo.springboot;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;



@EnableAsync
@SpringBootApplication(scanBasePackages={"springbootdemo.springboot"})
public class BootApp
{
    public static void main( String[] args )
    {
		SpringApplication.run(BootApp.class, args); 
    }
    
   
}
