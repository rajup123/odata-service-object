package springbootdemo.springboot.controller;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.Document;
import org.wiztools.xsdgen.XsdGen;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import springboootdemo.springboot.dbStatement.DBOperations;
import springbootdemo.springboot.entity.Employee;
import sun.misc.BASE64Encoder;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(path = "/")
public class BootController {
	
	DBOperations db =null;
	FileOutputStream fop = null;
	File file;
	
	@RequestMapping(method = RequestMethod.GET, path = "/hello")
	public void welcomepage() {
		
		System.out.println("Welcome To Spring boot App demo test");
		
	}	
	

	@RequestMapping(method = RequestMethod.POST, path = "/saveEmployee") 
   
	public ResponseEntity<?> saveEmployeeData(@RequestBody Employee empl) {
		Employee emp = new Employee();
		emp.setFirstname(empl.getFirstname());
		emp.setLastname(empl.getLastname());
		emp.setEmail(empl.getEmail());
		db = new DBOperations();
		db.insertIntoDB(emp);
		 return new ResponseEntity<Employee>(emp, HttpStatus.OK);
		
	}
	
	@RequestMapping(method = RequestMethod.GET, path = "/getEmployee") 
	public ResponseEntity<?> getEmployeeData() {
		System.out.println("get method being called from angular app");
		Employee emp = new Employee();		
		db = new DBOperations();
		emp =db.getEmployeeFromDB();
		 return new ResponseEntity<Employee>(emp, HttpStatus.OK);
		
	}
	
	
	@RequestMapping(method = RequestMethod.GET, path = "/getServerData" , produces=MediaType.APPLICATION_XML_VALUE) 
	public void getDataFromService() throws org.wiztools.xsdgen.ParseException, SAXException, ParserConfigurationException, TransformerException {
		 String url = "https://my338431.crm.ondemand.com/sap/c4c/odata/cust/v1/oppwithparty/OpportunityPartyCollection?$filter";
	        String name = "bhushan01usr";
	        String password = "Welcome1";
	        String authString = name + ":" + password;
	        String authStringEnc = new BASE64Encoder().encode(authString.getBytes());
	        System.out.println("Base64 encoded auth string: " + authStringEnc);
	        Client restClient = Client.create();
	        WebResource webResource = restClient.resource(url);
	        ClientResponse resp = webResource.accept("application/xml")
	                                         .header("Authorization", "Basic " + authStringEnc)
	                                         .get(ClientResponse.class);
	        
	        if(resp.getStatus() != 200){
	            System.err.println("Unable to connect to the server");
	        }      	        
	        
	        BufferedWriter bf;		
	        
	        try {			
	        	 String output = resp.getEntity(String.class);	       
	        	 System.out.println("response: "+output);
	        	
	        	 Document doc = convertStringToDocument(output);
	        	 System.out.println(stringToDom(output));	 	       
	        	 	DOMSource source = new DOMSource(doc);
	        	    FileWriter writer = new FileWriter(new File("c:/users/u62805/output.xml"));
	        	    StreamResult result = new StreamResult(writer);
	        	    TransformerFactory transformerFactory = TransformerFactory.newInstance();
	        	    Transformer transformer = transformerFactory.newTransformer();
	        	    transformer.transform(source, result);
	        	 
	        	 //System.out.println("converted string to document "+ doc);
		        File outputfile = new File("c:/users/u62805/newTestXml2.xml");   
		        bf = new BufferedWriter(new FileWriter(outputfile,true));
				 bf.write(output);
		        
				File inputfile = new File("c:/users/u62805/newFile.xml");
				InputStream inputStream= new FileInputStream(inputfile);
				Reader reader = new InputStreamReader(inputStream,"UTF-8");
				InputSource is = new InputSource(reader);
				is.setEncoding("UTF-8");			
		        String xsd= new XsdGen().parse(inputfile).toString();				 
				 file = new File("newfileXsd.xsd");
				 fop = new FileOutputStream(file);
				 if (!file.exists()) {
					 file.createNewFile();
					 }
				 byte[] xsdInBytes = xsd.getBytes();
				 fop.write(xsdInBytes);
				 
				fop.flush();
				fop.close();

				System.out.println("Done");

			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					if (fop != null) {
						fop.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		
	        
	       
	       
	       
		
		
		
	}
	
	private Document stringToDom(String output)throws SAXException, IOException, ParserConfigurationException {
		 DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder builder = factory.newDocumentBuilder();
	        return builder.parse(new InputSource(new StringReader(output)));
		
	}


	private static Document convertStringToDocument(String output) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
        DocumentBuilder builder;  
        try  
        {  
            builder = factory.newDocumentBuilder();  
            Document doc = builder.parse( new InputSource( new StringReader( output ) ) ); 
            return doc;
        } catch (Exception e) {  
            e.printStackTrace();  
        } 
        return null;
    }	
	
	
	/*@RequestMapping(method = RequestMethod.POST, path = "/helloAngular", produces=MediaType.APPLICATION_JSON_VALUE,consumes=MediaType.APPLICATION_JSON_VALUE)
	public String angularWorld(@RequestBody Employee emp) {
		System.out.println(emp.getFirstname());
		System.out.println("getting called ?");
		
		return "ok";
		
	}
	*/
		/*@POST
	    @Path("/helloEmployee")
	    @Produces(MediaType.APPLICATION_JSON_VALUE)
	    @Consumes(MediaType.APPLICATION_JSON_VALUE)
	    public JSONObject sayPlainTextHello(@RequestParam("employeeData")JSONObject inputJsonObj) {
			
			System.out.println("just checking");

	        String input = (String) inputJsonObj.get("input");
	        String output="The input you sent is :"+input;
	        JSONObject outputJsonObj = new JSONObject();
	        outputJsonObj.put("output", output);

	        return outputJsonObj;
	    }
	*/
	

}
