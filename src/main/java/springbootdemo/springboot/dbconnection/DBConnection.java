package springbootdemo.springboot.dbconnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

	static final String JDBC_DRIVER = "oracle.jdbc.OracleDriver";  
	   static final String DB_URL = "jdbc:oracle:thin:@//pdbdbtst.corp.netapp.com:7020/t1pdb.corp.netapp.com";

	   //  Database credentials
	   static final String USER = "MYNETAPP2";
	   static final String PASS = "we!c0me123";
	   static final String SCHEMA="mynetapp2";
	   Connection conn = null;
	
	
	public Connection getDBConnection() {
		
		try{
		    
		      Class.forName("oracle.jdbc.OracleDriver");
		      conn = DriverManager.getConnection(DB_URL, USER, PASS);
		      System.out.println("Database created successfully...");
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
		      
		      try{
		         if(conn==null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
		
		return conn;
	}
	
}
